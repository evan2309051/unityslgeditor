﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DataModel
{
	public class CharacterScriptableObject : ScriptableObject
	{
		public int version;
		
		public List<Character> characterList ;
		
		public Dictionary <string,Character> characterData ;
		
		public Character character;
		
		CharacterScriptableObject ()
		{
			version = 0;
			characterList = new List<Character> ();
			characterData = new Dictionary<string, Character> ();
		}
	}
}