namespace GRID_SYSTEM
{
	/**
	 * 
	 */
	[System.Serializable]
	public enum GRID_ATTRIBUTE_TYPE 
	{
		None,
		//ViewObject,
		AtkUp,
		AtkDown,
		DefUp,
		DefDown,
		IntUp,
		IntDown,
		ResUp,
		ResDown,
		SpdUp,
		SpdDown,
		CantMove,
		Link,
	}
}