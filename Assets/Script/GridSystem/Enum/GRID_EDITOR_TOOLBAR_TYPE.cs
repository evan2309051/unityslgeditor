﻿using UnityEngine;
using System.Collections;

public enum GRID_EDITOR_TOOLBAR_TYPE
{
	SwitchPower = 0,
	Select =1,
	BrushesAttribute = 2,
	LayerAttribute = 3,
}
