
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
/**
 * 
 */
namespace GRID_SYSTEM
{
	public interface IGridSystem {

		/**
		 * 
		 */
		Action<Vector3[]> OnFindedPath{get;}
		Action OnFindPathFalled{get;}

		/**
		 * @param Vector2 startPoint 
		 * @param Vector2 endPoint
		 */
		Vector3[] SearchPath(Vector2 startPoint, Vector2 endPoint);

		/**
		 * @param Vector2 inPoint
		 */
		LayerData[] GetLayerEffect(Vector2 inPoint);

	}
}