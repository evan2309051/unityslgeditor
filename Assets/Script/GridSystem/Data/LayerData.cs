
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace GRID_SYSTEM
{
	[System.Serializable]
	public class LayerData 
	{	
		public List<LayerAttribute> Attributes = new List<LayerAttribute>();
		public bool Enable = true;
		public LayerData()
		{

		}
		public LayerAttribute GetAttributeByType(GRID_ATTRIBUTE_TYPE type)
		{
			for(int i = 0 ; i < Attributes.Count ; ++i)
			{
				if( Attributes[i].PowerType == type)
					return Attributes[i];
			}
			return null;
		}
		public bool CheckHaveAttribute(GRID_ATTRIBUTE_TYPE type)
		{
			for(int i = 0 ; i < Attributes.Count ; ++i)
			{
                if( Attributes[i].PowerType == type)
					return true;
			}
			return false;
		}
		public bool DeleteAttribute(GRID_ATTRIBUTE_TYPE type)
		{
			for(int i = 0 ; i < Attributes.Count ; ++i)
			{
				if( Attributes[i].PowerType != type)
					continue;
				Attributes.RemoveAt(i);
				return true;
			}
			return false;
		}
	}
	//
}