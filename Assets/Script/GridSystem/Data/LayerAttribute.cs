﻿using System;
using UnityEditor;
using UnityEngine;
using GRID_SYSTEM;
namespace GRID_SYSTEM
{
	[System.Serializable]
	public class LayerAttribute
	{
		public static LayerAttribute CreateAttributeByType(GRID_ATTRIBUTE_TYPE type , float value)
		{
			switch(type)
			{
				case GRID_ATTRIBUTE_TYPE.Link:
					return new LayerActionAttribute(type , value);

			}
			return new LayerAttribute(type , value);
		}
		//
		public GRID_ATTRIBUTE_TYPE PowerType;
		public float PowerValue;
		public LayerAttribute(GRID_ATTRIBUTE_TYPE type , float value)
		{
			PowerType = type;
			PowerValue = value;
		}
	}
}