﻿using UnityEngine;
//using UnityEditor;
using System.Collections;
using System.Collections.Generic;
namespace GRID_SYSTEM
{
	[System.Serializable]
	public class LayerActionAttribute : LayerAttribute 
	{
		public List<GridVector2> SendToVector = new List<GridVector2>();
		public LayerActionAttribute(GRID_ATTRIBUTE_TYPE type , float value) : base(type , value)
		{
		}

	}
}