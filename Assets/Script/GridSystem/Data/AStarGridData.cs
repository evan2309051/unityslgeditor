
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
/**
 * 
 */
public class AStarGridData {
	/**
	 * 
	 */
	public int[] NodePoint;
	public AStarGridData ParentNode;
	public float HCost;

	/**
	 * 
	 */
	public float GCost;

	/**
	 * 
	 */
	public float FCost{
		get
		{
			return HCost + GCost;
		}
	}

	/**
	 * 
	 */
	public bool CloseGrid;
	public AStarGridData(int pointX , int pointY)
	{
		NodePoint = new int[]{ pointX , pointY};
	}
	public void ResetNode()
	{
		ParentNode = null;
		GCost = 0;
		HCost = 0;
	}
}