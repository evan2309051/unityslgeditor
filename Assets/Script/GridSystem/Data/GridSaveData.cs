using System.Collections;
using System.Collections.Generic;
namespace GRID_SYSTEM
{
	[System.Serializable]
	public class GridSaveData  
	{
		public string DataName;
		public int GridWidth;
		public int GridHeight;
		//public GRID_ATTRIBUTE_TYPE[] LayerNames;
		public string[] LayerNames;
		public GRID_RANGE_TYPE RangeType;
		public LayerData[][][] LayerDatas;
	}
}
