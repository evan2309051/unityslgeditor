//#define HaveLoadGrid
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GRID_SYSTEM
{
	/**
	 * 
	 */
	public class GridSystem :MonoBehaviour, IGridSystem 
	{
		public GRID_MODE GridMode;

		/**
		 * 
		 */
		private  GridContainer gridControl;

		/**
		 * 
		 */
		public  Action<Vector3[]> OnFindedPath
		{
			get
			{
				return onFindedPath;
			}
			set
			{
				onFindedPath = value;
			}
		}
		private Action<Vector3[]> onFindedPath;
		public Action OnFindPathFalled
		{
			get
			{
				return onFindPathFalled;
			}
			set
			{
				onFindPathFalled = value;
			}
		}
		private Action onFindPathFalled;

		public void Start()
		{
			switch(GridMode)
			{
				case GRID_MODE.AStarMode:
#if HaveLoadGrid
#else
				gridControl = new AStarGridContainer(GridCommonStat.BaseGridMaxWidth , GridCommonStat.BaseGridMaxHeight);
#endif
				break;
				case GRID_MODE.NavigationMode:
				break;
			}
		}
		/*
		 * @param Vector2 StartPoint 
		 * @param Vector2 endPoint
		 */
		public Vector3[] SearchPath(Vector2 startPoint, Vector2 endPoint)
		{
			Vector3[] result =  gridControl.SearchPath(startPoint , endPoint);
			if(result == null && OnFindPathFalled != null)
				OnFindPathFalled();
			else if(result !=null && OnFindedPath != null)
			{
				OnFindedPath(result);
			}
			return null;
		}
		/**
		 * @param Vector2 inPoint
		 */
		public LayerData[] GetLayerEffect(Vector2 inPoint)
		{

			return gridControl.GetLayerEffect(inPoint);
		}
		//
	}
}