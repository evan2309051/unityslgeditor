#define DEBUG
using UnityEngine;
using System.Collections;

namespace GRID_SYSTEM
{
	[RequireComponent(typeof(BoxCollider))]
	public class BoxGridControl : GridControl {

		BoxCollider boxcollider;

		// Use this for initialization
		public override void Start ()
		{
			base.Start ();
			boxcollider = this.GetComponent<BoxCollider>();
			boxcollider.size = new Vector3(GridCommonStat.BaseUnit , 0.2f , GridCommonStat.BaseUnit);
			this.transform.position = new Vector3(IndexPoint.x * GridCommonStat.BaseUnit , 0 , IndexPoint.y * GridCommonStat.BaseUnit);
		
#if DEBUG
			DebugAddLayer();
#endif
		} 
		#if DEBUG
		private LineRenderer debugLineRender;
		public void Update()
		{
			CreateLineRender();
		}
		public void CreateLineRender()
		{
			if(debugLineRender != null)
				return;
			debugLineRender = gameObject.AddComponent<LineRenderer>();
			debugLineRender.SetWidth(0.25f,0.25f);
			debugLineRender.SetVertexCount(5);
			SetDebugPoint();
		}
		public void SetDebugPoint()
		{
			debugLineRender.SetPosition(0 , new Vector3(RealPosition.x - GridCommonStat.BaseHalfUnit , 0 , RealPosition.y - GridCommonStat.BaseHalfUnit) );
			debugLineRender.SetPosition(1 , new Vector3(RealPosition.x + GridCommonStat.BaseHalfUnit , 0 , RealPosition.y - GridCommonStat.BaseHalfUnit) );
			debugLineRender.SetPosition(2 , new Vector3(RealPosition.x + GridCommonStat.BaseHalfUnit , 0 , RealPosition.y + GridCommonStat.BaseHalfUnit) );
			debugLineRender.SetPosition(3 , new Vector3(RealPosition.x - GridCommonStat.BaseHalfUnit , 0 , RealPosition.y + GridCommonStat.BaseHalfUnit) );
			debugLineRender.SetPosition(4 , new Vector3(RealPosition.x - GridCommonStat.BaseHalfUnit , 0 , RealPosition.y - GridCommonStat.BaseHalfUnit) );
		}
		public void DebugAddLayer()
		{
			//LayerData layerData = new LayerData(EFFECT_POWER_TYPE.AtkUp , IndexPoint.x + IndexPoint.y);
			//layerData.PowerType = EFFECT_POWER_TYPE.AtkUp;
			//AddLayer(layerData);
		}
		#endif

	}
}