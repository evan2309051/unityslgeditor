
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace GRID_SYSTEM
{
	/**
	 * 
	 */
	public abstract class GridContainer 
	{
		public int MaxWidth;
		public int MaxHeight;
		/**
		 * @param Vector2 startPoint  
		 * @param Vector2 endPoint
		 */
		public virtual Vector3[] SearchPath(Vector2 startPoint , Vector2 endPoint) 
		{
			// TODO implement here
			return null;
		}
		/**
		 * @param Vector2 point
		 */
		public virtual GridControl GetGridDataByScreenPoint() 
		{
			// TODO implement here
			return null;
		}

		/**
		 * @param Vector2 inPoint
		 */
		public virtual LayerData[] GetLayerEffect( Vector2 inPoint) 
		{
			// TODO implement here
			return null;
		}

	}
}