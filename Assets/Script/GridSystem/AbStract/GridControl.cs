using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace GRID_SYSTEM
{
	/**
	 * 
	 */
	public abstract class GridControl : MonoBehaviour
	{
		/**
		 * 
		 */
		protected Dictionary < GRID_ATTRIBUTE_TYPE , LayerData> layers = new Dictionary<GRID_ATTRIBUTE_TYPE, LayerData>();//LayerData[] layers;
		/*
		public float totalHeight
		{
			get
			{
				return 0;
			}
		}
		*/
		/**
		 * 
		 */
		public  Vector2 IndexPoint;
		/**
		 * 
		 */
		public Vector2 RealPosition
		{
			get
			{
				return new Vector2(IndexPoint.x * GridCommonStat.BaseUnit , IndexPoint.y * GridCommonStat.BaseUnit);
			}
		}

		/**
		 * 
		 */

		public virtual void Start()
		{
			gameObject.name = string.Format("GridNode[{0}],[{1}]" , IndexPoint.x.ToString() , IndexPoint.y.ToString());
		}

		//
		/**
		 * @param LayerData data
		 */
		public virtual void AddLayer( LayerData data) 
		{			
			// TODO implement here
			/*if(layers.ContainsKey(data.PowerType))
			{
				layers[data.PowerType] = data;
			}
			else
				layers.Add(data.PowerType , data);
*/
		}

		/**
		 * 
		 */
		public virtual LayerData[] GetAllLayer() 
		{
			// TODO implement here
			return layers.Values.ToArray();
		}
		public LayerData GetLayer(GRID_ATTRIBUTE_TYPE type)
		{
			if(!layers.ContainsKey(type))
				return null;
			else
				return layers[type];
		}

	}
}