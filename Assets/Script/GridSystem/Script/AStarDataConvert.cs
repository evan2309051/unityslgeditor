using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace GRID_SYSTEM
{
/**
 * 
 */
	public class AStarDataConvert {

		/**
		 * 
		 */
		public AStarDataConvert() 
		{
		}

		/**
		 * @param GridData[][] gridDatas
		 */
		public static AStarGridData[][] GridDataArrayToAStarDataArray( GridControl[][] gridDatas) 
		{
			// TODO implement here
			AStarGridData[][] result = new AStarGridData[gridDatas.Length][];
			for(int i  = 0; i < gridDatas.Length ; ++i)
			{
				result[i] = new AStarGridData[gridDatas[i].Length];
				for(int j  = 0 ; j < gridDatas.Length ; ++j)
				{
					result[i][j] = new AStarGridData(i , j);
					if(gridDatas[i][j].GetLayer(GRID_ATTRIBUTE_TYPE.CantMove) != null)
					{
						result[i][j].CloseGrid = true;
					}
					//
				}
			}
			return result;
		}

		/**
		 * @param GridData gridData
		 */
		public static AStarGridData GridDataToAStarData(GridControl gridData) 
		{
			//TODO implement here

			return null;
		}

	}
}