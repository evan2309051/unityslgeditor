using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace GRID_SYSTEM
{
	public class GridEditorResource 
	{
		private Dictionary<string , Texture>  resourceTexture = new Dictionary<string, Texture>();
		public GridEditorResource()
		{
			Load();
		}
		public void Load()
		{
			resourceTexture.Add( "HaveNot" ,  (Texture)Resources.Load("GridSystemEditorImage/GridIconHaveNot"));
			resourceTexture.Add( "Have" ,  (Texture)Resources.Load("GridSystemEditorImage/GridIconHave")); 
			resourceTexture.Add( "SelectGridBox" ,  (Texture)Resources.Load("GridSystemEditorImage/SelectGridBox"));
			resourceTexture.Add( "Gray" ,  (Texture)Resources.Load("GridSystemEditorImage/Gray")); //HaveAttribute_Icon
			resourceTexture.Add( "HaveAttribute" ,  (Texture)Resources.Load("GridSystemEditorImage/HaveAttribute_Icon"));
			resourceTexture.Add( GRID_EDITOR_TOOLBAR_TYPE.SwitchPower.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/SwitchPower"));
			resourceTexture.Add( GRID_EDITOR_TOOLBAR_TYPE.Select.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Select"));
			resourceTexture.Add( GRID_EDITOR_TOOLBAR_TYPE.BrushesAttribute.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Brushes"));
			resourceTexture.Add( GRID_EDITOR_TOOLBAR_TYPE.LayerAttribute.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Brushes"));
			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.None.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/None"));//Icon_Atk_Down
			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.AtkDown.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Icon_Atk_Down"));
			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.AtkUp.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Icon_Atk_Up"));

			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.DefUp.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Def_Up"));
			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.DefDown.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Def_Down"));

			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.IntUp.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Int_Up"));
			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.IntDown.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Int_Down"));

			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.ResUp.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Res_Up"));
			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.ResDown.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Res_Down"));

			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.SpdUp.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Spd_Up"));
			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.SpdDown.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Spd_Down"));
			resourceTexture.Add( GRID_ATTRIBUTE_TYPE.Link.ToString() ,  (Texture)Resources.Load("GridSystemEditorImage/Link"));

		}
		public Texture[] GetTextures(params string[] keys)
		{
			List<Texture> result = new List<Texture>();
			for(int i = 0 ; i < keys.Length ; ++i )
			{
				if(!resourceTexture.ContainsKey(keys[i]))
				{
					Debug.LogWarning(string.Format("Now this key :" , keys[i]));
					continue;
				}
				result.Add(resourceTexture[keys[i]]);
			}
			return result.ToArray();
		}
		public Texture GetTexture(string key)
		{
			if(!resourceTexture.ContainsKey(key))
				return null;
			return resourceTexture[key];
		}
	}
}