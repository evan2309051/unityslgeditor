
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GRID_SYSTEM
{
	[System.Serializable]
	public class GridVector2
	{
		public int X;
		public int Y;
		public GridVector2()
		{
		}
		public GridVector2(int x , int y)
		{
			X = x;
			Y = y;
		}
	}
	/**
	 * 
	 */
	public class GridCommonStat 
	{
		public static readonly float BaseUnit = 5;
		public static readonly float BaseHalfUnit = BaseUnit / 2f;
		public static readonly int BaseGridMaxWidth = 10;
		public static readonly int BaseGridMaxHeight = 10;
	}
}