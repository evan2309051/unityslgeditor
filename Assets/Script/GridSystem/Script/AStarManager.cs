
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
/**
 * 
 */
public class AStarManager {

	/**
	 * 
	 */
	private AStarGridData[][] aStarDatas;
	private List<AStarGridData> openList = new List<AStarGridData>();
	private List<AStarGridData> closeList =new List<AStarGridData>();
	private int[][] checkDirection = new int[][]
	{
		new int[]{0 , 1},
		new int[]{0 , -1},
		new int[]{1 , 0},
		new int[]{-1 , 0},
	};
	/**
	 * @param AStarData[][] aStarDatas
	 */
	public AStarManager(AStarGridData[][] datas) 
	{
		// TODO implement here
		this.aStarDatas = datas;
	}

	/**
	 * @param Vector2 startPoint  
	 * @param endPoint
	 */
	public Vector3[] AstarSearchPath(Vector2 startPoint ,Vector2 endPoint) 
	{
		// TODO implement here
		if(startPoint == endPoint)
		{
			Debug.Log("Is Same Point");
			return null;
		}
		if(startPoint.x < 0 || startPoint.x >= aStarDatas.Length ||
		   startPoint.y < 0 || startPoint.y >= aStarDatas[(int)startPoint.x].Length)
		{
			Debug.Log("Not this Point");
			return null;
		}
		if(endPoint.x < 0 || endPoint.x >= aStarDatas.Length ||
		   endPoint.y < 0 || endPoint.y >= aStarDatas[(int)endPoint.x].Length)
		{
			Debug.Log("Not this Point");
			return null;
		}
		openList.Clear();
		closeList.Clear();
		ResetAStarData();
		//
		AStarGridData nextNode = aStarDatas[(int)startPoint.x][(int)startPoint.y];
		nextNode.GCost = 0;
		nextNode.HCost = Mathf.Abs(endPoint.x - nextNode.NodePoint[0]) + Mathf.Abs(endPoint.y - nextNode.NodePoint[1]);
		GetNewOpenList(nextNode , endPoint);
		closeList.Add(nextNode);
		//
		while(true)
		{
			nextNode = null;
			if(openList.Count == 0)
				return null;
			for(int i = 0 ; i < openList.Count ; ++i)
			{
				if(nextNode == null)
				{
					nextNode = openList[i];
				}
				else
				{
					if(openList[i].FCost < nextNode.FCost)
						nextNode = openList[i];
				}
			}

			if(nextNode.NodePoint[0] == (int)endPoint.x &&
			   nextNode.NodePoint[1] == (int)endPoint.y)
			{
				Debug.Log("Find!");
				return FindRoot(nextNode);
			}
			if(openList.Contains(nextNode))
				openList.Remove(nextNode);
			closeList.Add(nextNode);
			GetNewOpenList(nextNode , endPoint);
		}
		//return null;
	}

	public void GetNewOpenList(AStarGridData openNode , Vector2 endPoint)
	{
		int distance = 1;
		for(int i = 0 ; i < checkDirection.Length ; ++i)
		{
			int[] nextPoint = new int[2]
			{
				openNode.NodePoint[0] + checkDirection[i][0],
				openNode.NodePoint[1] + checkDirection[i][1],
			};
			if(nextPoint[0] < 0 || nextPoint[0] >= aStarDatas.Length ||
			   nextPoint[1] < 0 || nextPoint[1] >= aStarDatas[(int)nextPoint[0]].Length)
			{
				continue;
			}
			AStarGridData nextNode = aStarDatas[nextPoint[0]][nextPoint[1]];
			if(nextNode == openNode.ParentNode||
			   nextNode.CloseGrid)
				continue;
			//
			float newHCost = Mathf.Abs(endPoint.x - nextNode.NodePoint[0]) + Mathf.Abs(endPoint.y - nextNode.NodePoint[1]);
			float newGCost = nextNode.GCost + distance;
			float newFCost = newHCost + newGCost;
			//
			if(openList.Contains(nextNode) &&
			   newFCost > nextNode.FCost)
			{
				continue;
			}
			else if(closeList.Contains(nextNode))
			{
				if(newFCost > nextNode.FCost)
					continue;
				closeList.Remove(nextNode);
				openList.Add(nextNode);
			}
			else
				openList.Add(nextNode);
			nextNode.GCost = newGCost;
			nextNode.HCost = newHCost;
			nextNode.ParentNode = openNode;
		}
	}
	public Vector3[] FindRoot(AStarGridData data)
	{
		List<Vector3> result = new List<Vector3>();
		while(data.ParentNode != null)
		{
			Debug.Log(string.Format("[{0}][{1}]", data.NodePoint[0] ,data.NodePoint[1]));
			result.Add(new Vector3( data.NodePoint[0], 0 ,data.NodePoint[1]));
			data = data.ParentNode;
		}
		result.Reverse();
		return result.ToArray();
	}
	public void ResetAStarData()
	{
		for(int i = 0 ; i < aStarDatas.Length ; ++i)
		{
			for(int j = 0 ; j < aStarDatas[i].Length ; ++j)
			{
				aStarDatas[i][j].ResetNode();
			}
		}
	}

	/**
	 * @param AStarData[][] aStarDatas
	 */
	public void RefreshAllAStarData(AStarGridData[][] newDatas) 
	{
		// TODO implement here
		//return null;
		aStarDatas = newDatas;
	}

	/**
	 * @param Vector2 changePoint
	 */
	public void RefreshAStarData(Vector2 changePoint , AStarGridData data) 
	{
		// TODO implement here
		if(!CheckArrayRange(changePoint))
			return;
		aStarDatas[(int)changePoint.x][(int)changePoint.y] = data;

	}
	protected bool CheckArrayRange(Vector2 point)
	{
		//bool result = false;
		if( point.x < 0 || point.x >= aStarDatas.Length ||
		   point.y < 0 || point.y >= aStarDatas[(int)point.x].Length)
		{
			return false;
		}
		return true;
	}
}