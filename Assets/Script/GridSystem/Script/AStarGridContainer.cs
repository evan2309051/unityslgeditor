
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace GRID_SYSTEM
{
	public class AStarGridContainer : GridContainer 
	{
		/**
		 * 
		 */
		public GRID_RANGE_TYPE RangeType = GRID_RANGE_TYPE.Box;
		public AStarManager AStarManager;
		/**
		 * 
		 */
		public GridControl[][] AStarGridDatas;

		/**
		 * 
		 */
		public AStarGridContainer(int maxGridWidth , int maxGridHeight) 
		{
			// TODO implement here
			this.MaxWidth = maxGridWidth;
			this.MaxHeight = maxGridHeight;
			CreateGridNodeArray(maxGridWidth , maxGridHeight);

			CreateAStarManager();
			//AStarManager.AstarSearchPath(new Vector2(0 , 0) ,  new Vector2( 0 , 11));
		}
		public override Vector3[] SearchPath (Vector2 startPoint, Vector2 endPoint)
		{
			return AStarManager.AstarSearchPath(startPoint ,  endPoint);
			//return base.SearchPath (startPoint, endPoint);
		}
		/**
		 */
		public  void CreateAStarManager() 
		{
			// TODO implement here
			AStarManager = new AStarManager(AStarDataConvert.GridDataArrayToAStarDataArray(AStarGridDatas));
		}


		/**
		 * @param int maxWidth 
		 * @param int maxHeight
		 */
		public void CreateGridNodeArray( int maxGridWidth,  int maxGridHeight) 
		{
			// TODO implement here
			if(GetGridSaveData() != null)
			{
				CreateGridDataBySaveData(GetGridSaveData());
				return;
			}
			GridSystem gridSystem = (GameObject.FindObjectOfType(typeof(GridSystem)) as GridSystem);
			if(gridSystem == null)
			{
				Debug.Log("NotGridSystem");
				return;
			}
			//
			AStarGridDatas = new GridControl[maxGridWidth][];
			for(int i = 0 ; i < maxGridWidth ; ++i)
			{
				AStarGridDatas[i] = new GridControl[maxGridHeight];
				for(int j = 0 ; j < maxGridHeight ; ++j)
				{
					AStarGridDatas[i][j] = CreateGridNode(gridSystem.gameObject , new Vector2(i , j));
				}
			}
			//
		}
		public GridControl CreateGridNode(GameObject parentObj,Vector2 point)
		{
			switch(RangeType)
			{
			case GRID_RANGE_TYPE.Box:
				BoxGridControl nodeControl = new GameObject().AddComponent<BoxGridControl>();
				//nodeControl.gameObject.AddComponent<BoxCollider>();
				nodeControl.transform.parent = parentObj.transform;
				nodeControl.IndexPoint = point;
				return nodeControl;
			case GRID_RANGE_TYPE.Hexagon:

				break;
			}
			return null;
		}
		public override LayerData[] GetLayerEffect (Vector2 inPoint)
		{
			int pointX = (int) inPoint.x;
			int pointY = (int) inPoint.y;
			if(pointX < 0 || pointX >= MaxWidth ||
			   pointY < 0 || pointY >= MaxHeight)
			{
				Debug.Log("Not this point");
			}
			return AStarGridDatas[pointX][pointY].GetAllLayer();
			//return base.GetLayerEffect (inPoint);
		}
		public void CreateGridDataBySaveData(GridSaveData saveData)
		{

			//
			GridSystem gridSystem = (GameObject.FindObjectOfType(typeof(GridSystem)) as GridSystem);
			if(gridSystem == null)
			{
				Debug.Log("NotGridSystem");
				return;
			}
			GridControl[][] newNodes;
			newNodes =new GridControl[saveData.LayerDatas.Length][];
			for(int i = 0 ; i < saveData.LayerDatas.Length ; ++i)
			{
				newNodes[i] = new GridControl[saveData.LayerDatas[i].Length];
				for(int j = 0 ; j < saveData.LayerDatas[i].Length ; ++j)
				{
					newNodes[i][j] = CreateGridNode(gridSystem.gameObject , new Vector2(i , j));
					for(int h = 0 ; h < saveData.LayerDatas[i][j].Length ; ++h)
					{
						newNodes[i][j].AddLayer(saveData.LayerDatas[i][j][h]);
					}
				}
			}
		}
		public GridSaveData GetGridSaveData()
		{

			return null;
		}
		public override GridControl GetGridDataByScreenPoint ()
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit)) 
			{
				Transform target = hit.transform;
				if(target.tag != "GridSystem")
					return null;
				return target.GetComponent<GridControl>();
			}
			return null;
		}
		//
	}
}