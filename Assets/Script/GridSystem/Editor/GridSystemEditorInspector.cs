﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
namespace GRID_SYSTEM
{
	public class GridSystemEditorInspector : EditorWindow 
	{
		// 0 is grid Attribute 1 is All Grid Attribute sum
		int currentDisplayMode = 0;
		string[] displayModeNames = new string[2]{"Attribute" , "Final Attribute"}; 
		public GUIStyle guiStyle;
		public static GridSystemEditorInspector Instance;
		private GRID_ATTRIBUTE_TYPE currentAddAttribute;
		GridSystemEditor gridSystemEditor;
		[MenuItem("SSLG Maker/GridSystem/GridEditorInspector")]
		static void Init()
		{
			Instance = (GridSystemEditorInspector)EditorWindow.GetWindow (typeof (GridSystemEditorInspector));
		}

		public void OnEnable()
		{
			Instance = this;
			autoRepaintOnSceneChange = true;
		}
		public void ResetData()
		{
			currentDisplayMode = 0;
		}
		public void SetListData(GridSystemEditor data)
		{
			ResetData();
			gridSystemEditor = data;
		}
		public void OnGUI()
		{
			if(gridSystemEditor == null)
				return;
			if(!gridSystemEditor.IsSelectGridNode)
				return;
			currentDisplayMode = GUILayout.Toolbar(currentDisplayMode , displayModeNames , EditorStyles.toolbarButton);
			switch(currentDisplayMode)
			{
				case 0:
					LayerData layer = gridSystemEditor.GridLayerDatas[(int)gridSystemEditor.CurrentSelectGrid.x][(int)gridSystemEditor.CurrentSelectGrid.y][(int)gridSystemEditor.CurrentSelectGrid.z];
					DrawAddAttribute(layer);
					DrawSelectLayers(layer);
                break;
                case 1:
					DrawFinalAttribute();
                break;
			}

		}
		public void DrawAddAttribute(LayerData layer)
		{
			GUILayout.BeginHorizontal();
			currentAddAttribute = (GRID_ATTRIBUTE_TYPE)EditorGUILayout.EnumPopup(currentAddAttribute , GUILayout.Width(150f));
			if(GUILayout.Button("Add" , GUILayout.Width(50f)))
			{
				if(CheckHaveAttribute(layer.Attributes , currentAddAttribute))
					return;
				layer.Attributes.Add(LayerAttribute.CreateAttributeByType(currentAddAttribute , 0));
				gridSystemEditor.Repaint(); 
			}
			GUILayout.EndHorizontal();
		}
		public void DrawSelectLayers(LayerData layer)
		{
			for(int i = 0 ; i < layer.Attributes.Count ; ++i)
			{
				LayerAttribute attribute = layer.Attributes[i];
				GUILayout.BeginVertical(EditorStyles.textArea);

				//attribute.DrawAttributeInpector(gridSystemEditor.EditResource , layer.Enable);
				DrawAttributeByType(attribute , layer.Enable);
				if(GUILayout.Button("Delete" , GUILayout.Width(80f)))
				{
					layer.Attributes.RemoveAt(i);
					gridSystemEditor.Repaint();
					break;
				}
				GUILayout.EndVertical();
			}
		}
		void DrawFinalAttribute()
		{
			List<LayerData> layers = gridSystemEditor.GridLayerDatas[(int)gridSystemEditor.CurrentSelectGrid.x][(int)gridSystemEditor.CurrentSelectGrid.y];
			Dictionary<GRID_ATTRIBUTE_TYPE , LayerAttribute> finalAttribute = new Dictionary<GRID_ATTRIBUTE_TYPE, LayerAttribute>();
			for(int i = 0 ; i <layers.Count ; ++i)
			{
				for(int j = 0 ; j < layers[i].Attributes.Count ; ++j)
				{
					if(finalAttribute.ContainsKey(layers[i].Attributes[j].PowerType))
						finalAttribute[layers[i].Attributes[j].PowerType] = layers[i].Attributes[j];
					else
						finalAttribute.Add(layers[i].Attributes[j].PowerType , layers[i].Attributes[j]);
				}
			}
			foreach(LayerAttribute attribute in finalAttribute.Values)
			{
				GUILayout.BeginHorizontal(EditorStyles.textArea);
				GUILayout.Button(gridSystemEditor.EditResource.GetTexture(attribute.PowerType.ToString()) , GUILayout.Width(50) , GUILayout.Height(50));
				GUILayout.BeginVertical();
				GUILayout.Label(string.Format("Type : {0}" , attribute.PowerType));
				GUILayout.BeginHorizontal();
				GUILayout.Label("Value : " , GUILayout.Width(50f));
				GUILayout.Label(attribute.PowerValue.ToString());
				GUILayout.EndHorizontal();
				GUILayout.EndVertical();
				GUILayout.EndHorizontal();
			}
        }
        public bool CheckHaveAttribute(List<LayerAttribute> attributes , GRID_ATTRIBUTE_TYPE beCheckType)
		{
			for(int i = 0 ; i < attributes.Count ; ++i)
			{
				if(attributes[i].PowerType == beCheckType)
				{
					EditorUtility.DisplayDialog("Have this Type",
					                            "Please Check Current Layer Attribute",
					                            "OK");
					return true;
				}
			}
			return false;
		}
		#region DrawAttribute
		void DrawAttributeByType(LayerAttribute attribute , bool isEnable)
		{
			GUILayout.BeginHorizontal();
			if(GUILayout.Button(gridSystemEditor.EditResource.GetTexture(attribute.PowerType.ToString()) , GUILayout.Width(50) , GUILayout.Height(50)))
			{
				
			}
			if(!isEnable)
				GUI.DrawTexture(GUILayoutUtility.GetLastRect() , gridSystemEditor.EditResource.GetTexture("Gray"));
			GUILayout.BeginVertical();
			GUILayout.Label(string.Format("Type : {0}" , attribute.PowerType));
			//
			switch(attribute.PowerType)
			{
				case GRID_ATTRIBUTE_TYPE.Link:
					DrawActionAttribute(attribute , isEnable);
				break;
				default:
					DrawBaseAttribute(attribute , isEnable);
				break;
			}
			//
			GUILayout.EndVertical();
			GUILayout.EndHorizontal();
		}
		void DrawBaseAttribute(LayerAttribute attribute , bool isEnable)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("Value : " , GUILayout.Width(50f));
			attribute.PowerValue =  EditorGUILayout.FloatField(attribute.PowerValue);
			GUILayout.EndHorizontal();
		}
		void DrawActionAttribute(LayerAttribute attribute , bool isEnable)
		{
			LayerActionAttribute actionAttribute = (LayerActionAttribute)attribute;
			if(actionAttribute == null)
				return;
			if(GUILayout.Button("Add" , GUILayout.Width(60f)))
			{
				GridSelectWindow.ShowGridSelectWindow((Vector2 point) => {actionAttribute.SendToVector.Add(new GridVector2((int)point.x , (int)point.y )); });
			}
			for(int i = 0 ; i < actionAttribute.SendToVector.Count ; ++i)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Label("ID:" + i.ToString() ,GUILayout.Width(80f));
				GUILayout.Label("X: " , GUILayout.Width(30f));
				GUILayout.Label(actionAttribute.SendToVector[i].X.ToString() , EditorStyles.textField , GUILayout.Width(80f));
				GUILayout.Label("Y: " , GUILayout.Width(30f));
				GUILayout.Label(actionAttribute.SendToVector[i].Y.ToString() , EditorStyles.textField , GUILayout.Width(80f));
				if(GUILayout.Button("一" , EditorStyles.miniButton , GUILayout.Width(20f)))
				{
					actionAttribute.SendToVector.RemoveAt(i);
					Repaint();
					break;
				}
				GUILayout.EndHorizontal();
			}

		}
		#endregion
		//
	}
}