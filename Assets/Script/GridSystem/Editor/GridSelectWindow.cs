﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
namespace GRID_SYSTEM
{
	public class GridSelectWindow : EditorWindow {
		Action<Vector2> selectGridComplect;
		public static void ShowGridSelectWindow(Action<Vector2> action)
		{

			GridSelectWindow window = (GridSelectWindow)EditorWindow.GetWindow (typeof (GridSelectWindow) , false ,"SelectGrid");
			window.position = new Rect(10 , 50 , 1024 , 768);
			window.selectGridComplect = action;
		}
		private bool isSelectMouse = false;
		public void OnGUI()
		{
			if(Event.current.type == EventType.MouseDown)
				isSelectMouse = true;
			if(GridSystemEditor.Instance == null)
			{
				GUI.Label(this.position , "Now Not Open GridEditor" , EditorStyles.boldLabel);
				return;
			}

			GridSystemEditor gridEditor = GridSystemEditor.Instance;
			Texture haveTex = gridEditor.EditResource.GetTexture("Have");
			GUILayout.BeginHorizontal(EditorStyles.textArea , GUILayout.Width(51.0f * gridEditor.GridWidth));
			for(int i = 0 ; i < gridEditor.GridLayerDatas.Length ; ++i)
			{
				GUILayout.BeginVertical(GUILayout.Height(51.0f * gridEditor.GridHeight));
				for(int j = 0 ; j < gridEditor.GridLayerDatas[i].Length ; ++j)
				{
					GUILayout.Box(haveTex , GUILayout.Width(50.0f) , GUILayout.Height(50.0f));
					EditorGUIUtility.AddCursorRect(GUILayoutUtility.GetLastRect() , MouseCursor.Link);
					if(GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition)&&
					   isSelectMouse)
					{
						if(selectGridComplect != null)
							selectGridComplect(new Vector2(i , j));
						Close();
					}
				}
				GUILayout.EndVertical();

			}
			GUILayout.EndHorizontal();
			if(Event.current.type == EventType.MouseUp)
				isSelectMouse = false;
		}
	}
}