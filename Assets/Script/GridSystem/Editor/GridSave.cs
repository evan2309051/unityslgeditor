﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using GRID_SYSTEM;
public class GridSave 
{
	public static void Save(GridSaveData data , string fileName)  
	{
		string filepath = EditorUtility.SaveFilePanel("Save Open Map" , "" , fileName + ".gridMap", "gridMap"); 
		//savedGames.Add(Game.current);
		BinaryFormatter bf = new BinaryFormatter();

		FileStream file = File.Create (filepath);
		if(filepath == string.Empty)
			return;
		bf.Serialize(file, data);
		file.Close();
	}
	public static GridSaveData Load() 
	{
		GridSaveData result = null;
		BinaryFormatter bf = new BinaryFormatter();
		string filepath = EditorUtility.OpenFilePanel("Select Open Map" , "" , "gridMap");
		if(filepath == string.Empty)
			return null;
        FileStream file = File.Open(filepath, FileMode.Open);
		result = (GridSaveData)bf.Deserialize(file);
		file.Close();
		return result;
	}

}
