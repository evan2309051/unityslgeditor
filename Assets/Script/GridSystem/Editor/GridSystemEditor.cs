﻿using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;
using System.Collections.Generic;
using GRID_SYSTEM;
public class GridSystemEditor : EditorWindow 
{
	public static GridSystemEditor Instance;
	public GridEditorResource EditResource;
	string dataName = string.Empty;
	public int GridWidth = 10;
	public int GridHeight = 10;
	int currentSelectLayerIndex = 0;
	int brushesPowerValue = 0;
	GRID_ATTRIBUTE_TYPE currentBrushesAttribute;
	//
	Texture gridIconHaveNot;
	Texture gridIconHave;
	public List<LayerData>[][] GridLayerDatas = null;
	List<string> showLayerNames = new List<string>();
	bool ifShowGrid = false;
	bool ifMouseDown = false;
	bool ifReverseBrushes = false;
	public bool IsSelectGridNode = false;
	float currentShowScale = 1;
	//
	GRID_EDITOR_TOOLBAR_TYPE currentGridToolBarType = GRID_EDITOR_TOOLBAR_TYPE.SwitchPower;
	Rect currentSelectGridRect;
	public Vector3 CurrentSelectGrid;
	[MenuItem("SSLG Maker/GridSystem/GridEditor")]
	static void ShowGridEditorWindow()
	{
		Instance = (GridSystemEditor)EditorWindow.GetWindow (typeof (GridSystemEditor));
	}
	void OnEnable()
	{
		Instance = this;
		autoRepaintOnSceneChange = true;
		ResetData();
		EditResource = new GridEditorResource();
		gridIconHaveNot = EditResource.GetTexture("HaveNot");
		gridIconHave    = EditResource.GetTexture("Have");
	}
	void OnGUI () 
	{
		if (Event.current.type == EventType.MouseDown)
		{
			ifMouseDown = true;
			Repaint ();
		}
		if (Event.current.type == EventType.ContextClick)
		{
			CreateContextMenu();
		}

		if(!ifShowGrid)
			ShowCreateUI();
		else
		{
			ShowGridBaseDataView();
			ShowGridToolBar();
			ShowLayerTableView();

			if(currentGridToolBarType != GRID_EDITOR_TOOLBAR_TYPE.BrushesAttribute)
				ShowGrid();
			else
				ShowBrushesGrid();
			ShowGridDataControl();
		}
		GUI.DrawTexture(currentSelectGridRect , EditResource.GetTexture("SelectGridBox"));
		if (Event.current.type == EventType.MouseUp)
			ifMouseDown = false;
	}
	void ShowCreateUI()
	{
		GUILayout.BeginVertical(GUILayout.Width(500));
		GUILayout.BeginHorizontal();
		GUILayout.Label("Name:" , GUILayout.Width(100));
		dataName = GUILayout.TextField(dataName);
		GUILayout.EndHorizontal();

		if(dataName == string.Empty)
			EditorGUILayout.HelpBox("Please Into FileName" , MessageType.Warning);
		GUILayout.BeginHorizontal();
		GUILayout.Label("Width:" , GUILayout.Width(100));
		string width = GUILayout.TextField(GridWidth.ToString());
		int.TryParse(width , out GridWidth);
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Height:" , GUILayout.Width(100));
		string height = GUILayout.TextField(GridHeight.ToString());
		int.TryParse(height , out GridHeight);
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		if(dataName != "" && GridWidth != 0 && GridHeight != 0)
		{
			if(GUILayout.Button("Create" , GUILayout.Width(100)))
			{
				CreateGrid();
			}
		}
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
	}
	void ShowGridBaseDataView()
	{
		GUILayout.BeginVertical(EditorStyles.textArea);
		GUILayout.Label(string.Format("Name : {0}" , dataName));
		GUILayout.Label(string.Format("Winth : {0}" , GridWidth));
		GUILayout.Label(string.Format("Height : {0}" , GridHeight));
		GUILayout.EndVertical();
	}
	void ShowLayerTableView()
	{
		GUILayout.BeginHorizontal(EditorStyles.textArea);
		int nextLayerIndex = GUILayout.Toolbar(currentSelectLayerIndex , showLayerNames.ToArray() , EditorStyles.toolbarButton, GUILayout.Width(showLayerNames.Count * 100));
		if(nextLayerIndex != currentSelectLayerIndex)
		{
			currentSelectLayerIndex = nextLayerIndex;
			ResetSelectGrid();
		}
		//
		if(GUI.changed &&
		   Event.current.button == 1)
		{
			CreateLayerToolBarContextMenu();
		}
		//
		if(GUILayout.Button("Add New Layer" ,GUILayout.Width(100)))
		{
			showLayerNames.Add(string.Format("Layer{0}" , showLayerNames.Count));
			AddGridLayer();
		}
		GUILayout.EndHorizontal();
	}
	void ShowGridToolBar()
	{
		GUILayout.BeginHorizontal();
		currentGridToolBarType = (GRID_EDITOR_TOOLBAR_TYPE)GUILayout.Toolbar((int)currentGridToolBarType ,
		                                                                     EditResource.GetTextures(GRID_EDITOR_TOOLBAR_TYPE.SwitchPower.ToString() ,
																				                         GRID_EDITOR_TOOLBAR_TYPE.Select.ToString(),
																				                         GRID_EDITOR_TOOLBAR_TYPE.BrushesAttribute.ToString(),
		                         																		GRID_EDITOR_TOOLBAR_TYPE.LayerAttribute.ToString()) ,
		                                                                     GUILayout.Width(200)  , GUILayout.Height(50));
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label("ViewScale:" , GUILayout.Width(60f));
		float newScaleView = EditorGUILayout.Slider(currentShowScale , 0.5f , 3f ,GUILayout.Width(150f));
		GUILayout.EndHorizontal();
		if(currentGridToolBarType == GRID_EDITOR_TOOLBAR_TYPE.BrushesAttribute)
		{
			GUILayout.BeginHorizontal();
			currentBrushesAttribute = (GRID_ATTRIBUTE_TYPE) EditorGUILayout.EnumPopup(currentBrushesAttribute ,GUILayout.Width(100f));
			ifReverseBrushes = EditorGUILayout.Toggle(ifReverseBrushes , GUILayout.Width(20f));
			GUILayout.Label ("ReverseBrushes" , GUILayout.Width(100f));
			GUILayout.EndHorizontal();
			brushesPowerValue = EditorGUILayout.IntField("BrushesValue" , brushesPowerValue);
		}
		GUILayout.EndVertical();
		if(GUILayout.Button("Reset" ,EditorStyles.miniButton , GUILayout.Width(60f)))
			newScaleView = 1f;
		if(newScaleView != currentShowScale)
		{
			currentShowScale = newScaleView;
			ResetSelectGrid();
		}
		GUILayout.EndHorizontal();
	}
	void ShowGrid()
	{
		if(currentSelectLayerIndex >= showLayerNames.Count)
			return;
		GUILayout.BeginHorizontal(EditorStyles.textArea , GUILayout.Width(51.0f * GridWidth * currentShowScale));
		for(int i = 0 ; i < GridLayerDatas.Length ; ++i)
		{
			GUILayout.BeginVertical(GUILayout.Height(51.0f * GridHeight * currentShowScale));
			for(int j = 0 ; j < GridLayerDatas[i].Length ; ++j)
			{
				if(GridLayerDatas[i][j][currentSelectLayerIndex] == null)
				{
					Debug.LogWarning(string.Format("this point is null [{0}][{1}]" , i , j));
				}
				if(!GridLayerDatas[i][j][currentSelectLayerIndex].Enable)
					GUILayout.Box(gridIconHaveNot , GUILayout.Width(50.0f * currentShowScale ) , GUILayout.Height(50.0f * currentShowScale));
				else
					GUILayout.Box(gridIconHave , GUILayout.Width(50.0f * currentShowScale) , GUILayout.Height(50.0f * currentShowScale));

				if (Event.current.type == EventType.Repaint &&
				    ifMouseDown &&
				    GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
				{
					OnClickGridNode(i , j , currentSelectLayerIndex);
					ifMouseDown = false;
					Repaint();
				}
				if(GridLayerDatas[i][j][currentSelectLayerIndex].Attributes.Count > 0)
				{
					GUI.DrawTexture(GUILayoutUtility.GetLastRect() , EditResource.GetTexture("HaveAttribute"));
				}
				// 
			}
			GUILayout.EndVertical();
		}
		GUILayout.EndHorizontal();
	}
	public void ShowBrushesGrid()
	{
		if(currentSelectLayerIndex >= showLayerNames.Count)
			return;
		GUILayout.BeginHorizontal(EditorStyles.textArea , GUILayout.Width(51.0f * GridWidth * currentShowScale));
		for(int i = 0 ; i < GridLayerDatas.Length ; ++i)
		{
			GUILayout.BeginVertical(GUILayout.Height(51.0f * GridHeight * currentShowScale));
			for(int j = 0 ; j < GridLayerDatas[i].Length ; ++j)
            {
				bool haveAttribute = GridLayerDatas[i][j][currentSelectLayerIndex].CheckHaveAttribute(currentBrushesAttribute);
				if(haveAttribute)
				{
					GUILayout.Box(EditResource.GetTexture( currentBrushesAttribute.ToString()) , GUILayout.Width(50.0f * currentShowScale ) , GUILayout.Height(50.0f * currentShowScale));
					GUI.Label(GUILayoutUtility.GetLastRect() , GridLayerDatas[i][j][currentSelectLayerIndex].GetAttributeByType(currentBrushesAttribute).PowerValue.ToString() , EditorStyles.boldLabel);
				}
				else
					GUILayout.Box(gridIconHaveNot , GUILayout.Width(50.0f * currentShowScale ) , GUILayout.Height(50.0f * currentShowScale));

				if(ifMouseDown &&
				   GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition) &&

				   currentBrushesAttribute != GRID_ATTRIBUTE_TYPE.None)
				{
					if(!haveAttribute && !ifReverseBrushes)
						GridLayerDatas[i][j][currentSelectLayerIndex].Attributes.Add(LayerAttribute.CreateAttributeByType(currentBrushesAttribute , brushesPowerValue));
					else if(haveAttribute && ifReverseBrushes)
						GridLayerDatas[i][j][currentSelectLayerIndex].DeleteAttribute(currentBrushesAttribute);
					else if(haveAttribute)
						GridLayerDatas[i][j][currentSelectLayerIndex].GetAttributeByType(currentBrushesAttribute).PowerValue = brushesPowerValue;

					Repaint();
                }
			}
			GUILayout.EndVertical();
		}
		GUILayout.EndHorizontal();
	}
	void ShowLayerAttribute()
	{

	}
	void ShowGridDataControl()
	{
		GUILayout.BeginHorizontal();
		if(GUILayout.Button("Save" , GUILayout.Width(100)))
		{
			SaveData();
		}
		GUILayout.EndHorizontal();
	}
	//
	void ResetData()
	{
		ifShowGrid = false;
		showLayerNames.Clear();
		currentSelectLayerIndex = 0;
		brushesPowerValue = 0;
		currentGridToolBarType = GRID_EDITOR_TOOLBAR_TYPE.Select;
		ifReverseBrushes = false;
		ClearGrid();
		ResetSelectGrid();
	}
	void ResetSelectGrid()
	{
		IsSelectGridNode = false;
		currentSelectGridRect = new Rect(0 , 0 , 0 , 0);
		CurrentSelectGrid = Vector3.zero;
		if(GridSystemEditorInspector.Instance != null)
		{
			GridSystemEditorInspector.Instance.Repaint();
        }
    }
    void CreateGrid()
	{
		GridLayerDatas = new List<LayerData>[GridWidth][];
		for(int i = 0 ; i < GridLayerDatas.Length ; ++i)
		{
			GridLayerDatas[i] = new List<LayerData>[GridHeight];
			for(int j = 0 ; j < GridLayerDatas[i].Length ; ++j)
			{
				GridLayerDatas[i][j] = new List<LayerData>();
			}
		}
		ResetData();
		ifShowGrid = true;
	}
	void ClearGrid()
	{
		if(GridLayerDatas == null)
			return;
		for(int i = 0 ; i < GridLayerDatas.Length ; ++i)
        {
			for(int j = 0 ; j < GridLayerDatas[i].Length ; ++j)
            {
				GridLayerDatas[i][j].Clear();
			}
		}
	}
	void AddGridLayer()
	{
		if(GridLayerDatas == null)
			return;
		for(int i = 0 ; i < GridLayerDatas.Length ; ++i)
		{
			for(int j = 0 ; j < GridLayerDatas[i].Length ; ++j)
            {
				GridLayerDatas[i][j].Add(new LayerData());
			}
		}
	}
	void DeleteGridLayer()
	{
		if(GridLayerDatas == null)
			return ;
		if(currentSelectLayerIndex >= showLayerNames.Count)
			return;
		showLayerNames.RemoveAt(currentSelectLayerIndex);
		for(int i = 0 ; i < GridLayerDatas.Length ; ++i)
		{
			for(int j = 0 ; j < GridLayerDatas[i].Length ; ++j)
			{
				GridLayerDatas[i][j].RemoveAt(currentSelectLayerIndex);
			}
		}
		currentSelectLayerIndex = 0; 
	}
	void OnClickGridNode(int pointX , int pointY ,int pointZ )
	{
		if(GetLayerData(pointX , pointY , pointZ) == null)
			return;
		switch(currentGridToolBarType)
		{
			case GRID_EDITOR_TOOLBAR_TYPE.SwitchPower:
			SwitchLayerEnable( pointX ,  pointY , pointZ );
			break;
			case GRID_EDITOR_TOOLBAR_TYPE.Select:

				currentSelectGridRect = GUILayoutUtility.GetLastRect();
				CurrentSelectGrid = new Vector3(pointX , pointY , pointZ);
				IsSelectGridNode = true;
			break;
		}
		if(GridSystemEditorInspector.Instance != null)
		{
			GridSystemEditorInspector.Instance.SetListData(this);
			GridSystemEditorInspector.Instance.Repaint();
        }
    }
    void SwitchLayerEnable(int pointX , int pointY ,int pointZ )
	{
		try
		{
			GridLayerDatas[pointX][pointY][pointZ].Enable = !GridLayerDatas[pointX][pointY][pointZ].Enable;
		}
		catch
		{
			Debug.Log("CantChange");
		}
	}
	LayerData GetLayerData(int pointX , int pointY ,int pointZ)
	{
		try
		{
			return GridLayerDatas[pointX][pointY][pointZ];
		}
		catch
		{
			return null;
		}
	}
	//
	void SaveData()
	{
		int pointZ = showLayerNames.Count;
		LayerData[][][] saveLayers =new LayerData[GridWidth][][];
		for(int i = 0 ; i < saveLayers.Length ; ++i)
		{
			saveLayers[i] = new LayerData[GridHeight][];
			for(int j = 0 ; j < saveLayers[i].Length ; ++j)
			{
				saveLayers[i][j] = new LayerData[pointZ];
				for(int h = 0 ; h <saveLayers[i][j].Length ; h++)
				{
					if( GridLayerDatas[i][j][h] == null)
						saveLayers[i][j][h] = null;
					else
						saveLayers[i][j][h] = GridLayerDatas[i][j][h];
				}
			}
		}
		//
		GridSaveData saveData = new GridSaveData();
		saveData.DataName = dataName;
		saveData.GridWidth = GridWidth;
		saveData.GridHeight = GridHeight;
		saveData.LayerDatas = saveLayers;
		saveData.LayerNames = showLayerNames.ToArray();
		GridSave.Save(saveData , dataName);
	}
	void LoadData()
	{
		GridSaveData savedata =  GridSave.Load();
		if(savedata == null)
			return ;
		ResetData();
		ifShowGrid = true;
		dataName = savedata.DataName;
		GridWidth = savedata.GridWidth;
		GridHeight = savedata.GridHeight;
		//showLayerEffectToEnum = new List<GRID_ATTRIBUTE_TYPE>(savedata.LayerNames);

		showLayerNames = new List<string>(savedata.LayerNames);

		GridLayerDatas = new List<LayerData>[GridWidth][];
		for(int i = 0 ; i  < savedata.LayerDatas.Length ;i++ )
		{
			GridLayerDatas[i] = new List<LayerData>[GridHeight];
			for(int j = 0 ; j  < savedata.LayerDatas[i].Length ; j++ )
			{
				GridLayerDatas[i][j] = new List<LayerData>();
				for(int h = 0 ; h  < savedata.LayerDatas[i][j].Length ; h++ )
				{
					GridLayerDatas[i][j].Add(savedata.LayerDatas[i][j][h]);
				}
			}
		}
		//
	}
	void CreateContextMenu()
	{
		GenericMenu menu  = new GenericMenu ();
		menu.AddItem(new GUIContent("LoadFile") , false , LoadData);
		menu.AddItem(new GUIContent("CreateNew") , false , ResetData);
		menu.ShowAsContext();
	}
	void CreateLayerToolBarContextMenu()
	{
		GenericMenu menu  = new GenericMenu ();
		menu.AddItem(new GUIContent("DeleteLayer") , false , DeleteGridLayer);
		menu.AddItem(new GUIContent("ChangeName") , false , null);
		menu.ShowAsContext();
	}
}