﻿using UnityEngine;
using System.Collections;

namespace GameLogic
{
	public class LogicA : LogicBase
	{
		private int totalTurn = 10;
		private int currentTurn = 0;
	
		public override void StartGame ()
		{
			throw new System.NotImplementedException ();
		}
		
		public override void TurnRound ()
		{
			currentTurn ++;
		
			if (currentTurn >= totalTurn) {
				EndGame ();
			}
		}
		
		public override void EndGame ()
		{
			throw new System.NotImplementedException ();
		}
	}
}