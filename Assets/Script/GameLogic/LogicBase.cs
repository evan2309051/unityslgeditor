﻿using UnityEngine;
using System.Collections;

namespace GameLogic
{
	public abstract class LogicBase
	{
		public abstract void StartGame ();
		
		public abstract void TurnRound ();
		
		public abstract void EndGame ();
	}
}