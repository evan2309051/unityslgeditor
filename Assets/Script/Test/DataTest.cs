﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class DataTest : MonoBehaviour
{
	public Dictionary <string,int> tttt = new Dictionary<string, int>();
	
	void Update ()
	{
		if (Input.GetKeyUp (KeyCode.L)) {
			LoadData();
		}
		
		if (Input.GetKeyUp (KeyCode.I)) {
			GenerateData ();
		}
	}
	
	[MenuItem("Window/Do Something")]
	public static void GenerateData ()
	{
		DataModel.CharacterScriptableObject test = ScriptableObject.CreateInstance<DataModel.CharacterScriptableObject> ();
		
		for (int i = 0; i < 3; i++) {
			var character = new DataModel.Character ();
			character.hp = i * 6;
			test.characterData.Add (i.ToString ("00"), character);
		}
		
		for (int i = 0; i < 3; i++) {
			var character = new DataModel.Character ();
			character.hp = i * 5;
			test.characterList.Add (character);
		}
		
		test.version ++;
		
		AssetDatabase.CreateAsset (test, @"Assets\DataModel\Character\Resources\DataModel.CharacterScriptableObject.asset");
		
		AssetDatabase.SaveAssets ();
		
		LoadData ();
	}
	
	[MenuItem("Window/UpdateData")]
	public static void UpdateData ()
	{
		var test = Resources.Load<DataModel.CharacterScriptableObject> ("DataModel.CharacterScriptableObject");
		
		for (int i = 0; i < test.characterList.Count; i++) {
			test.characterList[i].hp = i * 100;
		}
		
		AssetDatabase.SaveAssets();
		
		test = null;
		
		Resources.UnloadUnusedAssets();
	}
	
	[MenuItem("Window/LoadData")]
	static void LoadData ()
	{
		Resources.UnloadUnusedAssets();
	
		var test = Resources.Load<DataModel.CharacterScriptableObject> ("DataModel.CharacterScriptableObject");
		
		Debug.Log ("LoadData dict => " + test.characterData.Count);
		
		Debug.Log ("LoadData list => " + test.characterList.Count);
	}
}
