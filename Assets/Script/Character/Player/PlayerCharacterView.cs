﻿using UnityEngine;
using System.Collections;

namespace Character{
	
	public class PlayerCharacterView : CharacterView {

		protected override void ChangeDirection(Vector3 lastPath,Vector3 newPath){
		
			Direction direction  = Direction.FORWARD;
			
			if(newPath.x - lastPath.x > 0)
				direction = Direction.RIGHT;
			else if(newPath.x - lastPath.x < 0)
				direction = Direction.LEFT;
			
			if(newPath.z - lastPath.z > 0)
				direction = Direction.FORWARD;
			else if(newPath.z - lastPath.z < 0)
				direction = Direction.BACK;
			
			float dir = 0f;
			
			switch(direction){
				
			case Direction.FORWARD :
				dir	 = 0f;
				break;
				
			case Direction.RIGHT :
				dir = 90f;
				break;
				
			case Direction.LEFT :
				dir = -90f;
				break;
				
			case Direction.BACK :
				dir = 180f;
				break;
				
				
			}
			
			this.gameObject.transform.rotation = Quaternion.Euler(0,dir,0);

		}
	}
}
