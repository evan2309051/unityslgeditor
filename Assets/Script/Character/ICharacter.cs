﻿using UnityEngine;
using System;
using System.Collections;

namespace Character{
	
	public interface ICharacter{

		void OnAttack();

		void OnMove(Vector3[] path);
	}

	/*public interface IMovement {
		
		void Move();
		
		void Attack();
	}*/

}
