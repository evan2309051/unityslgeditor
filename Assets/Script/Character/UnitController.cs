﻿using UnityEngine;
using System;
using System.Collections;

namespace Character{

	public class UnitController : MonoBehaviour ,ICharacter{

		public CharacterView unitView;

		private void Start(){

		}

		public void DeployUnitView (CharacterView view) {

			unitView = view;

		}

		public void OnAttack () {

			unitView.Attack();
		}

		public void OnMove (Vector3[] path) {

			unitView.Move(path);
		}
	}
}
