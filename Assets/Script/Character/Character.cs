﻿using System;
using System.Collections;

namespace Character{

	public class Character{ 

		//public CharacterView View {get;set;}
		public UnitController CharaController;

		public Character (UnitController chara)
		{
			CharaController = chara;

			HP = MAXHP;

			Level = 1;
		}

		//public Action OnMove;

		//public Action OnAttack;

		public Action OnDamage;

		public Action OnDied;

		public int Level { get; private set;}

		int _hp;

		public bool IsDead
		{
			get { return HP <= 0;}
		}

		public int HP 
		{
			get 
			{
				return _hp;
			}
			set
			{
				var oldValue = _hp;

				_hp = value;

				if (_hp < 0)
				{
					_hp = 0;
				}

				if (oldValue != _hp)
				{
					if (OnDamage != null) OnDamage();

					if (IsDead) 
					{
						if (OnDied != null) OnDied();
					}
				}
			}
		}

		public static int MAXHP = 100; 

	}
}
