﻿using UnityEngine;
using System;
using System.Collections;

namespace Character{

	public class CharacterView : MonoBehaviour {//,IMovement {

		public enum Direction {
			FORWARD,
			LEFT,
			RIGHT,
			BACK
		}//TODO:move to somwhere better

		private Animator _myAnimator;
			//private Vector3 oriPos;
		//private Quaternion oriRot;

		void Start () {

			//oriPos = this.gameObject.transform.position;
			
			//oriRot = this.gameObject.transform.rotation;
			
			_myAnimator = GetComponent<Animator>();
			
			if(_myAnimator == null){
				
				_myAnimator = this.gameObject.AddComponent<Animator>();
				
			}

		}

		public void Attack(){

			SetAnimatorState("Attack");
		}

		public void Move(Vector3[] path){

			StartCoroutine(MoveByStep(path));

		}

		IEnumerator MoveByStep(Vector3[] path){

			//SetAnimatorState("Walk 15 Sick Zombie");

			for(int i = 0; i < path.Length; i++){

				yield return new WaitForSeconds(0.5f);

				Vector3 lastPoint = new Vector3(0,0,0);

				if( i > 0)
					lastPoint = path[i - 1];
				else
					lastPoint = this.gameObject.transform.position;

				ChangeDirection(lastPoint,path[i]);

				this.gameObject.transform.position = path[i];

			}
		
		}

		protected virtual void ChangeDirection(Vector3 lastPath,Vector3 newPath){
		
			/*Direction direction  = Direction.FORWARD;

			if(newPath.x - lastPath.x > 0)
				direction = Direction.RIGHT;
			else if(newPath.x - lastPath.x < 0)
				direction = Direction.LEFT;

			if(newPath.z - lastPath.z > 0)
				direction = Direction.FORWARD;
			else if(newPath.z - lastPath.z < 0)
				direction = Direction.BACK;

			float dir = 0f;

			switch(direction){

				case Direction.FORWARD :
					dir	 = 0f;
				break;

				case Direction.RIGHT :
					dir = 90f;
				break;

				case Direction.LEFT :
					dir = -90f;
				break;

				case Direction.BACK :
					dir = 180f;
				break;


			}

			this.gameObject.transform.rotation = Quaternion.Euler(0,dir,0);*/

		}

	 	void SetAnimatorState(string name){
			
			//this.gameObject.transform.position = oriPos;
			
			//this.gameObject.transform.rotation = oriRot;
			
			int hash = Animator.StringToHash ("Base Layer."+ name);
			
			_myAnimator.Play(hash);
			
		}

	}
}
